#POM pour la page home
*** Settings ***
Library           SeleniumLibrary
Library    ../robot-env/Lib/site-packages/robot/libraries/String.py
*** Variables ***
*** Keywords ***

Chose Date
    [Arguments]    ${date_field_xpath}   ${day_of_week}   ${day}
    Click Element    ${date_field_xpath}
    Wait Until Element Is Visible    xpath=//td[contains(@class, 'xdsoft_day_of_week${day_of_week}') and contains(@class, 'xdsoft_date')]//div[text()='${day}']    15s
    Wait Until Element Is Enabled    xpath=//td[contains(@class, 'xdsoft_day_of_week${day_of_week}') and contains(@class, 'xdsoft_date')]//div[text()='${day}']    15s
    Click Element    xpath=//td[contains(@class, 'xdsoft_day_of_week${day_of_week}') and contains(@class, 'xdsoft_date')]//div[text()='${day}']
