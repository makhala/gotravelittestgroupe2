#POM pour la page home
*** Settings ***
#Importer la librairie Selénium
Library   SeleniumLibrary
Library    ../robot-env/Lib/site-packages/robot/libraries/XML.py
*** Variables ***
# ${} pour déclarer une variable
${Browser}        Chrome
${Link_Menu}   xpath=//*[@id='menu-primary-menu']/li/a
${BUTTON_LOGIN}   xpath://a[normalize-space()='Login']
${HOME_URL}        https://gotravel.ittestgroup.com/


*** Keywords ***

Go to URl 
   [Arguments]   ${url}  
   Open Browser  ${url}  ${Browser} 
   Maximize Browser Window

Go To Menu By name
   [Documentation]    Permet de Naviger vers les diferents Menus
   [Arguments]    ${page-NAME}

   @{Links_Menu} =  Get Webelements  ${Link_Menu}

    FOR    ${current_menu}    IN    @{Links_Menu}
       
       Wait Until Element Is Visible    ${current_menu}   5

       ${text_Link} =    Get Text    ${current_menu}

        IF   '${text_Link}' == '${page-NAME}'

           Log    ${text_Link}

           Click Element    ${current_menu}

           Exit For Loop
           
        END
   END

 Go to Login 

   Click Element    ${BUTTON_LOGIN} 


Get Current url

  ${current_url} =  Get Location 

  RETURN  ${current_url}
