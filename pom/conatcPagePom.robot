#POM pour les tests navigation
*** Settings ***

Library   SeleniumLibrary
Resource    homePagePom.robot
*** Variables ***

${nameTxt}      xpath://*[@id="wpcf7-f800-p46-o1"]/form/div[2]/div[1]/div[1]/p/span/input

${emailTxt}      xpath://*[@id="wpcf7-f800-p46-o1"]/form/div[2]/div[1]/div[2]/p/span/input

${subjectTxt}      xpath://*[@id="wpcf7-f800-p46-o1"]/form/div[2]/div[2]/p/span/input
${commentTxt}      xpath://*[@id="wpcf7-f800-p46-o1"]/form/div[2]/div[3]/p/span/textarea
${submit_btn}        xpath://*[@id="wpcf7-f800-p46-o1"]/form/div[2]/div[4]/p/button/span
${leave_message_msg}        xpath://div[@class='wpcf7-response-output']
${contact_url}        https://gotravel.ittestgroup.com/contact/

*** Keywords ***
Set And Submit Form
   [Documentation]  Remplir et soumettre un formulaire
   [Arguments]   ${name}    ${email}   ${Subject}  ${comment}
   Wait Until Element Is Visible  ${nameTxt}  10  
   Press Key    ${nameTxt}    ${name}
   Press Key    ${emailTxt}     ${email}
   Press Key    ${subjectTxt}     ${Subject}
   Press Key    ${commentTxt}     ${comment}
   Click Element    ${submit_btn}

Verify successfull Message
   [Documentation]  Verifiier le message de succès de soumission du formulaire
   [Arguments]   ${expected_txt}
   Wait Until Element Is Visible  ${leave_message_msg}     10  
   ${actual_txt} =   Get Text    ${leave_message_msg}
   Should Be Equal    ${actual_txt}    ${expected_txt}

Go To contacte Page
   Go to URl    https://gotravel.ittestgroup.com/
   Go To Menu By name    Contact