#POM pour les tests connexion
*** Settings ***

Library   SeleniumLibrary
Resource    homePagePom.robot
*** Variables ***
${email_user_field}        //input[@id='username']
${password_userfield}      //input[@id='password']
${connection_button}       //button[@name='login']
*** Keywords ***

Set And Submit Login Form
#Remplir et Soumettre le formulaire de connexion
    
   [Arguments]    ${email}   ${password} 

   Input Text    ${email_user_field}    ${email}

   Input Text    ${password_userfield}    ${password} 

   Click Element    ${connection_button}