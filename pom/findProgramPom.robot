#POM pour les test Recher de Programme
*** Settings ***
Library           SeleniumLibrary
*** Variables ***
${BROWSER}          Chrome
${HOME_PAGE_URL}    https://gotravel.ittestgroup.com/
${HOME_PAGE_LINKS}    xpath=//*[@id="menu-main-menu"]/li/a
${Bouton_Recherche_Locator}     xpath=//i[@class='icomoon icomoon-search']
*** Keywords ***
Click Find Circuit Button
    Wait Until Element Is Visible     ${Bouton_Recherche_Locator}       10   
    Click Element    ${Bouton_Recherche_Locator}