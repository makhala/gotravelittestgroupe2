#Connexion à un compte 
*** Settings ***
Library      SeleniumLibrary
Resource    ../../pom/homePagePom.robot
Resource    ../../pom/conatcPagePom.robot
*** Variables ***
${EMAIL}           epape@mbaye.com
${NAME}            Pape
${SUBJECT}         Need more information
${COMMENT}         This is a random comment
${SUCSESS_MESSAGE}        Thank you for your message. It has been sent.
${UNSUCSESS_MESSAGE}        One or more fields have an error. Please check and try again.
*** Test Cases ***
Set And Submit Form With requiered Field
    [Tags]    Contact Form
    [Documentation]      Test avec les champs obligatoire remplis
    Go To contacte Page
    Maximize Browser Window
    Set And Submit Form    ${NAME}    ${EMAIL}    ${SUBJECT}    ${COMMENT}
    Verify successfull Message    ${SUCSESS_MESSAGE}

Set And Submit Form Without Email
    [Tags]    Contact Form
    [Documentation]      Test avec les champs obligatoire non remplis
    Set And Submit Form       ${NAME}    ${null}    ${SUBJECT}    ${COMMENT}
    Verify successfull Message    ${UNSUCSESS_MESSAGE}
Set And Submit Form Without Unrequiered Field
    [Tags]    Contact Form
    [Documentation]      Test avec les champs non obligatoire non remplis
    Set And Submit Form    ${NAME}    ${EMAIL}    ${null}    ${null}
    Verify successfull Message    ${SUCSESS_MESSAGE}
    