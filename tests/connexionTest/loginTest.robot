#Connexion à un compte 
*** Settings ***
#Importer la librairie Selénium
Library      SeleniumLibrary
#Pour importer un autre fichier qui existe déja
Resource   ../../pom/ConnexionPagePom.robot
*** Variables ***
*** Test Cases ***
Connexion Avec Un Compte Existant
     [Documentation]    Connexion avec un compte existant
     [Tags]    Connexion
      Go to URl    url=https://gotravel.ittestgroup.com/
      Maximize Browser Window
      Go to Login
      Set And Submit Login Form    email=lydialounas1820@gmail.com    password=bonjour
      Wait Until Element Is Visible    //div[@class='woocommerce-notices-wrapper']//li[1]    5
      Element Should Contain    locator=//div[@class='woocommerce-notices-wrapper']//li[1]    expected=Vous êtes bien connecté!


Connexion Avec Un Compte Inexistant
     [Documentation]    Connexion avec un compte Inxistant
     [Tags]    Connexion
      Maximize Browser Window
      Set And Submit Login Form    email=pape@gmail.com    password=bonjour
      Wait Until Element Is Visible    //div[@class='woocommerce-notices-wrapper']//li[1]    5
      Element Should Contain    locator=//div[@class='woocommerce-notices-wrapper']//li[1]    expected=Adresse e-mail inconnue.
