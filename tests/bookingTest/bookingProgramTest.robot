#Réserver un programme sélectionné
*** Settings ***
Library    SeleniumLibrary
Resource    ../../pom/programmDetailsPagePom.robot
Resource    ../../pom/homePagePom.robot
*** Keywords ***
*** Variables ***
${COPENHAGEN_TOUR_URL}                https://gotravel.ittestgroup.com/product/copenhagen-city-tours-4-hours/
${AMSTERDAMLAKE_URL}            https://gotravel.ittestgroup.com/product/amsterdam-and-lake-ijssel-cycle/
${BROWSER}            chrome
${DATE_LOCATOR}        xpath://input[@name='ovabrw_pickup_date']
${BOOK__NOW_BUTTON_LOCATOR}        xpath://button[normalize-space()='Booking Now']
${CART_ACCESS_BUTTON_LOCATOR}        xpath://i[@class='ovaicon ovaicon-shopping-cart']
${DISPLAY_CART_BUTTON_LOCATOR}        xpath://a[@class='button wc-forward']
${COPENHAGEN_CITY_TOURS_LOCATOR}        xpath://a[contains(text(),'Copenhagen City Tours - 4 Hours')]
${AMSETRDAM_LAKE_LOCATOR}   xpath://a[contains(text(),'Amsterdam and Lake IJssel Cycle')]
*** Test Cases ***
Add Book Copenhagen City Tours To Cart
    [Documentation]   Ajout au panier Book Copenhagen City Tours Test case
    [Tags]    Booking
    Go to URl    ${COPENHAGEN_TOUR_URL}
    Maximize Browser Window
    Wait Until Element Is Visible    ${BOOK__NOW_BUTTON_LOCATOR}
    Chose Date    ${DATE_LOCATOR}     6    1
    Sleep  3
    Click Element    ${BOOK__NOW_BUTTON_LOCATOR}
    Sleep  3
    Mouse Over    ${CART_ACCESS_BUTTON_LOCATOR}
    Sleep  3
    Wait Until Element Is Visible     ${DISPLAY_CART_BUTTON_LOCATOR}    timeout=5
    Click Element    ${DISPLAY_CART_BUTTON_LOCATOR}
    Element Should Be Visible    ${COPENHAGEN_CITY_TOURS_LOCATOR}
Add Book Amsterdam Lake To Cart
    [Documentation]    Ajout au panier Amsterdam Lake Test case
    [Tags]    Booking
    Go to URl    ${AMSTERDAMLAKE_URL}
    Maximize Browser Window
    Wait Until Element Is Visible    ${BOOK__NOW_BUTTON_LOCATOR}
    Chose Date    ${DATE_LOCATOR}     6    1
    Sleep  3
    Click Element    ${BOOK__NOW_BUTTON_LOCATOR}
    Sleep  3
    Mouse Over    ${CART_ACCESS_BUTTON_LOCATOR}
    Sleep  3
    Wait Until Element Is Visible     ${DISPLAY_CART_BUTTON_LOCATOR}    timeout=5
    Click Element    ${DISPLAY_CART_BUTTON_LOCATOR}
    Element Should Be Visible    ${AMSETRDAM_LAKE_LOCATOR}