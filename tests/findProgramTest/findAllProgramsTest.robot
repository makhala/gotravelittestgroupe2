#Rechercher tous les programmes
*** Settings ***
Library      SeleniumLibrary
Resource     ../../pom/homePagePom.robot
Resource     ../../pom/findProgramPom.robot

*** Variables ***
${Find_Activities_URL}           https://gotravel.ittestgroup.com/tour-list/?ovabrw_destination=all&brw_tour_types_name=all&ovabrw_pickup_date=&ovabrw_adults=2&ovabrw_childrens=0&ovabrw_babies=0
${Generic_Destination_Locator}      //*[@id="brw-search-ajax-result"]/div/div/div[2]/div[1]/div[1]/span
*** Test Cases ***
Rechercher Tous Les Programs
        [Tags]    Recherche
        [Documentation]  Recherchez Tous les programmes
        Go To Url    ${HOME_PAGE_URL}
        Click Find Circuit Button
        ${ACTUEL_URL}=    Get Current Url
        Should Be Equal     ${ACTUEL_URL}         ${Find_Activities_URL}
