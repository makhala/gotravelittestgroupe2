#Naviguer vers différentes pages
*** Settings ***
Library      SeleniumLibrary
Resource    ../../pom/homePagePom.robot
*** Variables ***
${WELCOM_MESSAGE_LOCATOR}        xpath://h3[normalize-space()='We Are Welcome']
${TOURS_LIST_TITLE_LOCATOR}        xpath://h1[contains(@class,'header_title')]
${Blog_TITLE_LOCATOR}        xpath://h1[contains(@class,'header_title')]
${SHOP_TITLE_LOCATOR}        xpath://h1[@class=' header_title']
${EXPECTED_BLOC_TITLE}        Catégorie : Simple Products
${CONTACT_TITLE_LOCATOR}            xpath://h1[contains(@class,'header_title')]
${EXPECTED_CONTACT_TITLE}       Contact Us
*** Test Cases ***
Go To Home Section
    [Tags]    Navigation
    [Documentation]      Test la navigation vers la page Home
    Go to URl    url=${HOME_URL}
    Go To Menu By name    page-NAME=HOME
    Wait Until Element Is Visible    ${WELCOM_MESSAGE_LOCATOR}    5
    Element Should Contain    ${WELCOM_MESSAGE_LOCATOR}    expected=We Are Welcome
Go To Tours Section
    [Tags]    Navigation
    [Documentation]      Test la navigation vers la page Tours
    Go To Menu By name    page-NAME=Tours
    Wait Until Element Is Visible    ${TOURS_LIST_TITLE_LOCATOR}    5
    Element Should Contain    ${TOURS_LIST_TITLE_LOCATOR}    expected=Tour List
Go To Blog Section
    [Tags]    Navigation
    [Documentation]      Test la navigation vers la page Blog
    Go To Menu By name    page-NAME=Blog
    Wait Until Element Is Visible    ${Blog_TITLE_LOCATOR}    5
    Element Should Contain    ${Blog_TITLE_LOCATOR}    expected=Blog
Go To Shop Section
    [Tags]    Navigation
    [Documentation]      Test la navigation vers la page Shop
    Go To Menu By name    page-NAME=Shop
    Wait Until Element Is Visible    ${SHOP_TITLE_LOCATOR}    5
    ${Shop_Title_text}=    Get Text    ${SHOP_TITLE_LOCATOR}
    Should Be Equal    ${Shop_Title_text}    ${EXPECTED_BLOC_TITLE}
Go To Contact Section
    [Tags]    Navigation
    [Documentation]      Test la navigation vers la page Contact
    Go To Menu By name    page-NAME=Contact
    Wait Until Element Is Visible    ${CONTACT_TITLE_LOCATOR}    5
    ${Contact_Title_text}=    Get Text    ${CONTACT_TITLE_LOCATOR}
    Should Be Equal    ${Contact_Title_text}    ${EXPECTED_CONTACT_TITLE}
